"""Config."""
import os


DATAWAREHOUSE_URL = os.environ.get("DATAWAREHOUSE_URL")
DATAWAREHOUSE_TOKEN = os.environ.get("DATAWAREHOUSE_TOKEN")

MAILING_LIST_URL = os.environ.get("MAILING_LIST_URL")
COOKIE_PATH = '/tmp/dw-mailarchive-crawler-cookie'
CACHE_PATH = '/tmp/dw-mailarchive-crawler-cache'
