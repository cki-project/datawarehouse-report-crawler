#!/usr/bin/env python
"""Datawarehouse report crawler."""
import datetime
import logging

import config
import helpers
from classes import LatestMailArchiveCrawler, MMessageBase


def filter_messages(messages_list):
    """Remove unwanted messages."""
    messages = []

    for message in messages_list:
        # Filter out messages without pipeline id
        if not message.pipelineid:
            continue

        # Not a result email
        if not (
                'SKIPPED: ' in message.msg['Subject'] or
                'PASS: ' in message.msg['Subject'] or
                'PANICKED: ' in message.msg['Subject'] or
                'FAIL: ' in message.msg['Subject']):
            continue

        # Filter out messages older than 7 days.
        message_datetime = datetime.datetime.strptime(
            message.msg['X-List-Received-Date'],
            '%a, %d %b %Y %H:%M:%S %z'
        ).replace(tzinfo=None)
        if (datetime.datetime.now() - datetime.timedelta(days=7)) > message_datetime:
            continue

        # Fallback. Add to the list.
        messages.append(message)

    return messages


def process_message(message):
    """Process the email and report to DW if necessary."""
    try:
        reports = helpers.get_pipeline_reports(message.pipelineid)
    except Exception:  # pylint: disable=broad-except
        logging.error("Couldn't find pipeline %d in the DW", message.pipelineid)
        return

    if not reports:
        helpers.submit_report(message.pipelineid, message.msg)


def run():
    """Run, Forest, run!."""
    crawler = LatestMailArchiveCrawler(config.MAILING_LIST_URL, None, config.COOKIE_PATH)
    crawler.update_listarchive(config.CACHE_PATH)

    messages = MMessageBase.listarchives2msgs(config.CACHE_PATH)
    messages = filter_messages(messages)

    for message in messages:
        process_message(message)


if __name__ == '__main__':
    run()
