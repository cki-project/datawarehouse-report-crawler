"""Helpers."""
import json
import requests
import config

SESSION = requests.Session()
SESSION.headers.update({'User-Agent': 'reports-crawler'})


def get_pipeline_reports(pipeline_id):
    """Get reports from a pipeline."""
    response = SESSION.get(f'{config.DATAWAREHOUSE_URL}/api/1/pipeline/{pipeline_id}/report')
    assert response.status_code == 200
    return response.json()['results']['reports']


def submit_report(pipeline_id, email):
    """Send the sent emails to the Datawarehouse."""
    payload = {
        'content': email.as_string(),
    }

    headers = {
        'Authorization': f'Token {config.DATAWAREHOUSE_TOKEN}',
        'Content-Type': 'application/json'
    }

    response = SESSION.post(
        f'{config.DATAWAREHOUSE_URL}/api/1/pipeline/{pipeline_id}/report',
        data=json.dumps(payload), headers=headers
    )

    assert response.status_code == 201, response.reason
